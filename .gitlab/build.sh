#!/bin/bash

# exit when any command fails; be verbose
set -ex

CMS_PATH=/cvmfs/cms.cern.ch
CMSSW_RELEASE=CMSSW_10_6_8_patch1
export MY_BUILD_DIR=${PWD}

pwd # Debug
shopt -s expand_aliases
source ${CMS_PATH}/cmsset_default.sh
cd /home/cmsusr
cmsrel ${CMSSW_RELEASE}
cd ${CMSSW_RELEASE}/src
mkdir -p AnalysisCode
mv ${MY_BUILD_DIR}/ZPeakAnalysis AnalysisCode
cmsenv
scram b
